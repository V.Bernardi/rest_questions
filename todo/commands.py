from .app import app,db

@app.cli.command()
def createBD():
    ''' Création de toutes les tables de la BD à partir des models
    '''
    db.drop_all()
    db.create_all()
    print("BD créée !")
