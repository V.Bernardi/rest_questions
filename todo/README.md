# Pour lancer

faites un virtualenv avec le fichier "*requirements.txt*"
déplacer vous dans le fichier td2/ et lancer les commandes
*flask createbd* puis *flask run*
Pour Créer la bd puis lancer l'appli web 
 

# Pour ajouter des données dans la bd

entrez "*sqlite3 question.db*" dans le terminal

Puis les inserts suivants (par exemple):

insert into questionnaire values(1,"les tests");

insert into question values (1, "c'est un test ?", "simplequestion", 1, "oui", "non", false);

insert into question values (2, "voici un TEST ?", "simplequestion", 1, "et oui", "mais non", true);


# les Curls

## LES GETS

*Pour les questions*

curl -i http://127.0.0.1:5000/todo/api/v2.0/questions

curl -i http://127.0.0.1:5000/todo/api/v2.0/question/1

*Pour les questionnaires*

curl -i http://127.0.0.1:5000/todo/api/v2.0/questionnaires

curl -i http://127.0.0.1:5000/todo/api/v2.0/questionnaire/1


## LES DELETE

*Pour les questions*

curl -i -H "Content-Type:application/json" -X DELETE -d '{"id":"1"}' http://localhost:5000/todo/api/v2.0/question/supprimer/1


*Pour les questionnaires*

curl -i -H "Content-Type:application/json" -X DELETE -d '{"id":"1"}' http://localhost:5000/todo/api/v2.0/questionnaire/supprimer/1