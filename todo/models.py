from .app import db
from flask import jsonify, url_for
from sqlalchemy.sql.expression import func

class Questionnaire(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Questionnaire (%d) %s>" % (self.id, self.name)

    def to_json(self):
        json_questionnaire = {
            'url': url_for('un_questionnaire', idQuestionnaire=self.id, _external=True),
            'name': self.name,
            'questions': [ q.to_json()['url'] for q in self.questions ]
        }
        return json_questionnaire

class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    question_type = db.Column(db.String(12))
    questionnaire_id = db.Column(db.Integer, db.ForeignKey('questionnaire.id'))
    
    questionnaire = db.relationship("Questionnaire",backref=db.backref("questions", lazy="dynamic",cascade="all, delete-orphan"))

    def __repr__(self):
        return "<Question (%d) %s>" % (self.id, self.title)

    __mapper_args__={
        'polymorphic_identity':'question',  
        'with_polymorphic':'*',
        "polymorphic_on": question_type 
    }

    def to_json(self):
        json_quest = {
            'url': url_for('une_question',idQuest=self.id, _external=True),
            'questionnaire_url': url_for('un_questionnaire', idQuestionnaire=self.questionnaire_id, _external=True),
            'title': self.title,
            'question_type': self.question_type,
            'question': self.json_content()
        }
        return json_quest

class SimpleQuestion(Question):
    firstAlternative = db.Column(db.String(120))
    secondAlternative = db.Column(db.String(120))
    firstChecked = db.Column(db.Boolean)

    __mapper_args__={
        'polymorphic_identity':'simplequestion',
        'with_polymorphic':'*'
    }

    def json_content(self):
        json_quest = {}
        json_quest['firstAlternative'] = self.firstAlternative
        json_quest['secondAlternative'] = self.secondAlternative
        return json_quest


# QUESTIONS
def get_questions():
    return Question.query.all()

def get_id_questions():
    return Question.query.get(Question.id).all()

def get_question(idQuestion):
    return Question.query.filter(Question.id == idQuestion).first()

def get_max_question():
    # scalar pour passer du type querry à un type traitable ( ici int )
    return db.session.query(func.max(Question.id)).scalar()


# AJOUTER
def add_question(question):
    db.session.add(question)
    db.session.commit()

# SUPRIMER
def del_question(idQuest):
    question = get_question(idQuest)
    try:
        db.session.delete(question)
        db.session.commit()
    except:
        print("Pas de question ", idQuest, " a supprimer !")

#QUESTIONNAIRES
def get_questionnaires():
    return Questionnaire.query.all()

def get_questionnaire(idQuestionnaire):
    return Questionnaire.query.filter(Questionnaire.id == idQuestionnaire).first()

def get_max_questionnaire():
    return db.session.query(func.max(Questionnaire.id)).scalar()

def add_questionnaire(questionnaire):
    db.session.add(questionnaire)
    db.session.commit()


# SUPRIMER
def del_questionnaire(idQuestionnaire):
    questionnaire = get_questionnaire(idQuestionnaire)
    try:
        db.session.delete(questionnaire)
        db.session.commit()
    except:
        print("Pas de questionnaire ", idQuestionnaire, " a supprimer !")

