from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask ( __name__ )

import os.path

app.config.update(SECRET_KEY='POUET')
def mkpath(p):
    return os.path.join(os.path.dirname(__file__), p)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + mkpath('../question.db')
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
db = SQLAlchemy(app)