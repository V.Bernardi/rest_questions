from flask import jsonify , abort , make_response , request, url_for, redirect
from .app import app
from .models import *


#   GUETTEURS

# QUESTIONS
@app.route("/todo/api/v2.0/questions", methods = ["get"])
def liste_questions():
    questions=get_questions()
    return [question.to_json() for question in questions]

@app.route ("/todo/api/v2.0/question/<int:idQuest>", methods = ["get"])
def une_question(idQuest):
    question=get_question(idQuest)
    if question is None:
        abort(400)
    return question.to_json()

# QUESTIONNAIRES
@app.route ('/todo/api/v2.0/questionnaires', methods = ['GET'])
def liste_questionnaires():
    questionnaires=get_questionnaires()
    return [questionnaire.to_json() for questionnaire in questionnaires]

@app.route ('/todo/api/v2.0/questionnaire/<int:idQuestionnaire>', methods = ['GET'])
def un_questionnaire(idQuestionnaire):
    questionnaire =  get_questionnaire(idQuestionnaire)
    if questionnaire is None: abort(404)
    return questionnaire.to_json()


#   POSTIERS

@app.route ('/todo/api/v2.0/questionnaire/ajouter', methods = ['POST'])
def ajouter_questionnaire():
    if not request.json or not 'name' in request.json:
        abort (400)
    id = get_max_questionnaire() + 1
    questionnaire = {
        'id': id,
        'name': request.json['name'],
        'question':[]
    }
    add_questionnaire(questionnaire)
    return url_for('un_questionnaire', idQuestionnaire=id)


# DELETE
@app.route("/todo/api/v2.0/question/supprimer/<int:idQuest>", methods = ["DELETE"])
def supp_question(idQuest):
    del_question(idQuest)
    return url_for('liste_questions')

@app.route("/todo/api/v2.0/questionnaire/supprimer/<int:idQuestionnaire>", methods = ['DELETE'])
def supp_questionnaire(idQuestionnaire):
    del_questionnaire(idQuestionnaire)
    return url_for('liste_questionnaires')